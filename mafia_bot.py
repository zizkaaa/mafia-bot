import discord
import random
import os
import game_sub as gs
from discord.ext import commands
import time
client = commands.Bot(command_prefix='$', case_insensitive=True)


async def mute(ctx: commands.Context, state: bool) -> list:
    author = ctx.message.author
    vch = author.voice.channel
    players = list(filter(lambda x: x != client.user, vch.members))
    for player in players:
        await player.edit(mute=True)
        await player.edit(deafen=True)
    return players


@client.command()
async def load(ctx, ext):
    client.load_extension(f"cogs.{ext}")


@client.command()
async def unload(ctx, ext):
    client.unload_extension(f"cogs.{ext}")

for filename in os.listdir('./cogs'):
    if filename.endswith(".py"):
        client.load_extension(f'cogs.{filename[:-3]}')


@client.command()
async def game(ctx):
    author = ctx.message.author
    vch = author.voice.channel
    guild = ctx.message.author.guild
    players = list(filter(lambda x: x != client.user, vch.members))
    random.shuffle(players)
    p_dict = gs.distribute(players)
    non_maf = [i for i in players if i not in p_dict['mafia']]
    c_dict = gs.channel_index_by_name(guild.text_channels)
    for _ in range(4):
        await guild.create_role(name='‏‏‎ ‎')
    await guild.create_role(name='dead')
    roles_list = list(
        filter(lambda x: x.name == '‏‏‎ ‎' or x.name == 'dead', guild.roles))
    roles_list = sorted(roles_list, key=lambda x: x.name, reverse=True)
    for strength, role in enumerate(p_dict):
        for player in p_dict[role]:
            await player.add_roles(roles_list[strength])
    await gs.solve_perm(roles_list, guild.text_channels)
    don = random.choice(p_dict['mafia'])
    await author.voice.channel.connect(timeout=3600, reconnect=True)
    #Здесь цикл для игры ы
    while True:
        kill = gs.mafia_part(
            guild.text_channels[c_dict["mafia"]], don, non_maf)
        await kill.edit(mute=True)
        await kill.add_role()
client.run('NzQ5MzQ2MzY2NzkwMDQxNjgx.X0qpMA.nwTXKOrH3zqdw6Wt7Yf8q2oRbEw')
