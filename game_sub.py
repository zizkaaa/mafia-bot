import random
import math
import string
import discord as ds
all_roles = {"mafia": 0, "comissar": 1, "doctor": 2, "city": 3}
all_roles_inv = ["mafia", "comissar", "doctor"]


def rand_str(n): return ''.join(
    [random.choice(string.ascii_lowercase) for i in range(n)])


def set_choices(x: set, num: int) -> list:
    ret = random.choices(tuple(x), k=num)
    x = x-set(ret)
    return ret

def choose_one(x): return set_choices(x, 1)


def distribute(players: list) -> dict:
    distributed = {}
    m = len(players)//3
    random.shuffle(players)
    distributed.update({"mafia": players[0:m], "comm": [players[m]],
                        "doctor": [players[m+1]], "npc": players[m+2:]})
    return distributed


async def solve_perm(roles: list, texts: list):
    for channel in texts:
        if channel.name in all_roles_inv:
            for i, role in enumerate(roles):
                var = (all_roles[channel.name] == i)
                await channel.set_permissions(role, read_messages=var, send_messages=var)


def channel_index_by_name(channels: list) -> dict:
    return {channels[i].name: i for i in range(len(channels))}


async def mafia_part(maf_tex: ds.channel.TextChannel, don: ds.User, p_list: list) -> ds.User:
    while True:
        last_maf = maf_tex.last_message.author
        nick = maf_tex.last_message.content.lower()
        if last_maf == don and nick.startswith("убить"):
            player = list(filter(lambda x: nick in x.name.lower(), p_list))
            if player:
                return player[0]
        else:
            await maf_tex.send("Что то не так")
