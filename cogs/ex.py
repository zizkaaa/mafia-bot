import discord
from discord.ext import commands


class example(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        print("Bot is up and running")
        print('We have logged in as {0.user}'.format(self.client))

    @commands.command()
    async def ping(self, ctx):
        await ctx.send(f'{round(self.client.latency*1000)}ms')

    @commands.command()
    async def mute(self, ctx):
        author = ctx.message.author
        vch = author.voice.channel
        players = list(filter(lambda x: x != self.client.user, vch.members))
        for player in players:
            await player.edit(mute=True)
            await player.edit(deafen=True)

    @commands.command()
    async def unmute(self, ctx):
        author = ctx.message.author
        vch = author.voice.channel
        players = list(filter(lambda x: x != self.client.user, vch.members))
        for player in players:
            await player.edit(mute=False)
            await player.edit(deafen=False)

    @commands.command()
    async def clean(self, ctx):
        guild = ctx.message.author.guild
        roles_list = list(
            filter(lambda x: x.name == '‏‏‎ ‎' or x.name == 'dead', guild.roles))
        for role in roles_list:
            await role.delete()


def setup(client):
    client.add_cog(example(client))
import discord
from discord.ext import commands


class example(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        print("Bot is up and running")
        print('We have logged in as {0.user}'.format(self.client))

    @commands.command()
    async def ping(self, ctx):
        await ctx.send(f'{round(self.client.latency*1000)}ms')

    @commands.command()
    async def mute(self, ctx):
        author = ctx.message.author
        vch = author.voice.channel
        players = list(filter(lambda x: x != self.client.user, vch.members))
        for player in players:
            await player.edit(mute=True)
            await player.edit(deafen=True)

    @commands.command()
    async def unmute(self, ctx):
        author = ctx.message.author
        vch = author.voice.channel
        players = list(filter(lambda x: x != self.client.user, vch.members))
        for player in players:
            await player.edit(mute=False)
            await player.edit(deafen=False)

    @commands.command()
    async def clean(self, ctx):
        guild = ctx.message.author.guild
        roles_list = list(
            filter(lambda x: x.name == '‏‏‎ ‎' or x.name == 'dead', guild.roles))
        for role in roles_list:
            await role.delete()


def setup(client):
    client.add_cog(example(client))
